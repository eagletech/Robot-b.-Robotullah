
▄▄▄▄▄▄   ▄▄     ▄▄▄  ▄      ▄▄▄▄▄▄       ▄▄▄▄▄▄▄ ▄▄▄▄▄▄   ▄▄▄  ▄    ▄  
█        ██   ▄▀   ▀ █      █               █    █      ▄▀   ▀ █    █  
█▄▄▄▄▄  █  █  █   ▄▄ █      █▄▄▄▄▄          █    █▄▄▄▄▄ █      █▄▄▄▄█  
█       █▄▄█  █    █ █      █               █    █      █      █    █  
█▄▄▄▄▄ █    █  ▀▄▄▄▀ █▄▄▄▄▄ █▄▄▄▄▄          █    █▄▄▄▄▄  ▀▄▄▄▀ █    █  
													   
Source code of the robot for the Vex VRC 2022 - Competition Tipping Point. 
 
 											ROBOT b. ROBOTULLAH 


 CONTROLS

	Left analog controls the chase as intuitively as possible. 
	Right analog's Y axis controls the arm's movement, allowing to only stay within the 3 - 160 boundaries. 
	The up and down arrows control the arm, without respecting the boundaries.  
	L1 lifts up or down the arm according to the last position. 
	R1 runs the autonomous code. 
 

 Branch Documentation 
  main   - is the branch that has the last stably-working code of multi-threaded design as well as having the recorded autonomous feature that conflicts with the vexnet system. 
  dev    - the in-development possible future version of the main branch. 
  vexnet - the one-threaded, recorded-autonomous-lacking version of the software that was written at the second night of the competition seeing the conflictions between main and dev branchs' design chocies and the operation of vexnet.


Epilogue
	Pınar Hoca ve tüm Doğa Koleji nefret edicilerinin laneti üzerlerine olsun.
	Let the curse of Pınar Hoca and all that hate Doğa Koleji shall be upon them.
