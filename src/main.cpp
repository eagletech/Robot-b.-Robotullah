#include "../include/main.h"
															  
								//	▄▄▄▄▄▄   ▄▄     ▄▄▄  ▄      ▄▄▄▄▄▄       ▄▄▄▄▄▄▄ ▄▄▄▄▄▄   ▄▄▄  ▄    ▄
								//	█        ██   ▄▀   ▀ █      █               █    █      ▄▀   ▀ █    █
								//	█▄▄▄▄▄  █  █  █   ▄▄ █      █▄▄▄▄▄          █    █▄▄▄▄▄ █      █▄▄▄▄█
								//	█       █▄▄█  █    █ █      █               █    █      █      █    █
								//	█▄▄▄▄▄ █    █  ▀▄▄▄▀ █▄▄▄▄▄ █▄▄▄▄▄          █    █▄▄▄▄▄  ▀▄▄▄▀ █    █
															  
																							/* Source code of the robot for the Vex Robotics 2022 - Competition Tipping Point. */ 

																	/* ROBOT b. ROBOTULLAH */

																		/* CONTROLS */

										//	Left analog controls the chase as intuitively as possible.
										//	Right analog's Y axis controls the arm's movement, allowing to only stay within the 3 - 160 boundaries.
										//	The up and down arrows control the arm, without respecting the boundaries. 
										//	L1 lifts up or down the arm according to the last position.
										//	R1 runs the autonomous code.


using namespace pros;

void print(const char *msg, bool clear) {
	static int line = 0;
	if (clear)
		lcd::clear();

	lcd::set_text(++line, msg);
	puts(msg);
}


void initialize() {
	lcd::initialize();
	print("welcome my son");
	print("welcome to the machine");

  	imu.reset();

	chassis = new Chassis (
		new	Motor (13, E_MOTOR_GEARSET_18, false, E_MOTOR_ENCODER_DEGREES),
		new	Motor (2, E_MOTOR_GEARSET_18, false, E_MOTOR_ENCODER_DEGREES),
		new	Motor (5, E_MOTOR_GEARSET_18, false, E_MOTOR_ENCODER_DEGREES),
		
		new	Motor (19, E_MOTOR_GEARSET_18, true, E_MOTOR_ENCODER_DEGREES),
		new	Motor (18, E_MOTOR_GEARSET_18, true, E_MOTOR_ENCODER_DEGREES),
		new	Motor (6, E_MOTOR_GEARSET_18, true, E_MOTOR_ENCODER_DEGREES)
	);

	arm = new Arm {
		new Motor (17, E_MOTOR_GEARSET_36, true, E_MOTOR_ENCODER_DEGREES),
		new Motor (16, E_MOTOR_GEARSET_36, false, E_MOTOR_ENCODER_DEGREES),
	};


	chassis->left_back->set_brake_mode(pros::E_MOTOR_BRAKE_BRAKE);
	chassis->left_mid->set_brake_mode(pros::E_MOTOR_BRAKE_BRAKE);
	chassis->left_front->set_brake_mode(pros::E_MOTOR_BRAKE_BRAKE);
	chassis->right_back->set_brake_mode(pros::E_MOTOR_BRAKE_BRAKE);
	chassis->right_mid->set_brake_mode(pros::E_MOTOR_BRAKE_BRAKE);
	chassis->right_front->set_brake_mode(pros::E_MOTOR_BRAKE_BRAKE);

	arm->left->set_brake_mode(E_MOTOR_BRAKE_BRAKE);
	arm->right->set_brake_mode(E_MOTOR_BRAKE_BRAKE);
}


// may die soon or sooner
void autonomous() {
	/* hardcoded_autonomous_fn(); */
	arm->move(0);
	chassis->move(0, 0);
	while (competition::is_autonomous()) delay(DELAY);
}

// yes, this is that opcontrol
void opcontrol() {

	int power, turn, left, right;
	int arm_power, arm_state = ARM_DOWN;

	bool lift_in_progress = false;

	while(true) {

		if(master.get_digital_new_press(DIGITAL_R1)) {
			hardcoded_autonomous_fn();
		}
		
		power = master.get_analog(E_CONTROLLER_ANALOG_LEFT_Y);
		turn = master.get_analog(E_CONTROLLER_ANALOG_LEFT_X);

		left  = power + turn;
		right = power - turn;

		chassis->move(left, right);


		arm_power = master.get_analog(E_CONTROLLER_ANALOG_RIGHT_Y);

		if ((arm_power > 20 && arm->get_position() < 160 * ARM_GEAR_RATIO) ||
			(arm_power < -20 && arm->get_position() > 3 * ARM_GEAR_RATIO)) {
			arm->move(arm_power);
			lift_in_progress = false;
		}
		else if (master.get_digital(E_CONTROLLER_DIGITAL_UP)) {
			arm->move(MAX_ARM_VEL);
	    	lift_in_progress = false;
		}
		else if (master.get_digital(E_CONTROLLER_DIGITAL_DOWN)) {
			arm->move(-MAX_ARM_VEL);
			lift_in_progress = false;
		}
		else if (!lift_in_progress)
			arm->move(0);

		if (((arm->get_position() > 148 * ARM_GEAR_RATIO) && arm_state == ARM_UP) || ((arm->get_position() < 1 * ARM_GEAR_RATIO) && arm_state == ARM_DOWN))
			lift_in_progress = false;

		if (master.get_digital_new_press(E_CONTROLLER_DIGITAL_L1)) { 
				if (arm_state == ARM_UP) {
					arm->move_absolute(1 * ARM_GEAR_RATIO, MAX_ARM_VEL);
					arm_state = ARM_DOWN;
				}
				else if (arm_state == ARM_DOWN) {
					arm->move_absolute(150 * ARM_GEAR_RATIO, MAX_ARM_VEL);
					arm_state = ARM_UP;
				}
				lift_in_progress = true;
		}

		if (master.get_digital_new_press(E_CONTROLLER_DIGITAL_B)) arm->tare_position();

		delay(DELAY);
	}
}
