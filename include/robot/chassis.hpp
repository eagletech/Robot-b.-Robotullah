#ifndef CHASSIS_H
#define CHASSIS_H

#include "../api.h"
#include "global.h"

using namespace pros;

#define WHEEL_RAD 10.5
#define WHEEL_CIRCUMFERENCE (WHEEL_RAD * PI)

#define MAX_CHASSIS_VEL MAX_VEL


struct Chassis {
	Motor* left_front; // 13
	Motor* left_mid;   // 2
	Motor* left_back;  // 5

	Motor* right_front; // 19
	Motor* right_mid;   // 18
	Motor* right_back;  // 6

	
	void move(int left, int right) {
		left_front->move(left);
		left_mid->move(left);
		left_back->move(left);

		right_front->move(right);
		right_mid->move(right);
		right_back->move(right);
	}

	void move_relative(float left_cm, float right_cm, int velocity, int d=10) {
		/* 360   pi 2r */
		/* x	   cnt */
		/* x = cnt * 360 / (pi * 2r) */
		float left_units = left_cm * 360 / WHEEL_CIRCUMFERENCE;
		float right_units = right_cm * 360 / WHEEL_CIRCUMFERENCE;

		left_front->tare_position();
		left_mid->tare_position();
		left_back->tare_position();
		right_front->tare_position();
		right_mid->tare_position();
		right_back->tare_position();

		left_front->move_relative(left_units, velocity);
		left_mid->move_relative(left_units, velocity);
		left_back->move_relative(left_units, velocity);
		right_front->move_relative(right_units, velocity);
		right_mid->move_relative(right_units, velocity);
		right_back->move_relative(right_units, velocity);

		if (abs((int) left_units) < abs((int) right_units)) { 
			
			while( !((left_mid->get_position() < left_units + 5) && (left_mid->get_position() > left_units - 5)) ) delay(d);
			while ( !((right_mid->get_position() < right_units + 5) && (right_mid->get_position() > right_units - 5)) ) delay(d);
		}
		else {
			while ( !((right_mid->get_position() < right_units + 5) && (right_mid->get_position() > right_units - 5)) ) delay(d);
			while( !((left_mid->get_position() < left_units + 5) && (left_mid->get_position() > left_units - 5)) ) delay(d);
		}

	}


	void turn_around(float degrees, int velocity=90, int d=10) { // left is minus 
		int rvel, lvel; 
		const int err_rate = 3;

		if(degrees < 0) {
			lvel = -velocity;
			rvel = velocity;
		}
		else {
			lvel = velocity;
			rvel = -velocity;
		}

		degrees -= velocity / 9.5;
		m_controller.take();
		left_front->move(lvel);
		left_mid->move(lvel);
		left_back->move(lvel);
		right_front->move(rvel);
		right_mid->move(rvel);
		right_back->move(rvel);

		imu.tare();
		double yaw = imu.get_yaw();
		while( !(yaw > degrees - err_rate && yaw < degrees + err_rate) ) {
			delay(d); // IMU has 100 hertz refresh rate
			yaw = imu.get_yaw();
		}

		left_front->move(0);
		left_mid->move(0);
		left_back->move(0);
		right_front->move(0);
		right_mid->move(0);
		right_back->move(0);

		delay(d);

		m_controller.give();

	}


	Chassis(Motor* l1, Motor* l2, Motor* l3, Motor* r1, Motor* r2, Motor* r3) {
		left_front = l1;
		left_mid = l2;
		left_back = l3;

		right_front = r1;
		right_mid = r2;
		right_back = r3;

	}

} *chassis = new Chassis (
	new	Motor (13, E_MOTOR_GEARSET_18, false, E_MOTOR_ENCODER_DEGREES),
	new	Motor (2, E_MOTOR_GEARSET_18, false, E_MOTOR_ENCODER_DEGREES),
	new	Motor (5, E_MOTOR_GEARSET_18, false, E_MOTOR_ENCODER_DEGREES),

	new	Motor (19, E_MOTOR_GEARSET_18, true, E_MOTOR_ENCODER_DEGREES),
	new	Motor (18, E_MOTOR_GEARSET_18, true, E_MOTOR_ENCODER_DEGREES),
	new	Motor (6, E_MOTOR_GEARSET_18, true, E_MOTOR_ENCODER_DEGREES)
);
#endif
