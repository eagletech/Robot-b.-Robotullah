#ifndef ARM_H
#define ARM_H
#include "../api.h"
#include "global.h"

using namespace pros;

#define ARM_GEAR_RATIO (96. / 12.)

#define MAX_ARM_VEL MAX_VEL

#define ARM_UP   0
#define ARM_DOWN 1

struct Arm { 
	Motor* left;
	Motor* right;

	void move(int power) {
		if (abs(power) < 20)
			power = 0;
		this->left->move(power);
		this->right->move(power);
	}

	void move_absolute(float degrees, int power) {
		this->right->move_absolute(degrees * ARM_GEAR_RATIO, power);
		this->left->move_absolute(degrees * ARM_GEAR_RATIO, power);
	}

	void move_relative(float degrees, int power) {
		this->left-> move_relative(degrees * ARM_GEAR_RATIO, power);
		this->right->move_relative(degrees * ARM_GEAR_RATIO, power);
	}

	void tare_position(void) {
		this->left->tare_position();
		this->right->tare_position();
	}

	double get_position(void) {
		return left->get_position();
	}

	void lift_up_down(int mode) { 
		int power;
		float rotation = left->get_position();

		if (mode == ARM_UP) {
			left->move_absolute( 150 * ARM_GEAR_RATIO, MAX_ARM_VEL);
			right->move_absolute(150 * ARM_GEAR_RATIO, MAX_ARM_VEL);
			while(rotation < 148 * ARM_GEAR_RATIO) {
				rotation = left->get_position();
				if (!competition::is_autonomous()) {
						power = master.get_analog(E_CONTROLLER_ANALOG_RIGHT_Y);
						if(power > 20 || power < -20)  break;
				}
				delay(5);	
			}
		}
		else if (mode == ARM_DOWN) {
			left->move_absolute( 3 * ARM_GEAR_RATIO, MAX_ARM_VEL);
			right->move_absolute(3 * ARM_GEAR_RATIO, MAX_ARM_VEL);
			while(rotation > 3 * ARM_GEAR_RATIO) {
				rotation = left->get_position();
				if (!competition::is_autonomous()) {
						power = master.get_analog(E_CONTROLLER_ANALOG_RIGHT_Y);
						if(power > 20 || power < -20)  break;
				}
				delay(5);	
			}
		}
		master.set_text(0, 0, "çıktım abi");

		this->move(0);
		print("0ladim abi");

	}


	Arm(Motor* l, Motor* r) {
		left = l;
		right = r;
	}
	
}; 

Arm *arm;

#endif
