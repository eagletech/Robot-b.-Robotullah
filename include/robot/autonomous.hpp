#ifndef AUTONOMOUS_H
#define AUTONOMOUS_H

/* STRATEGIES */
/* Slot 1: Left Simple */
/* Slot 2: Right Simple */
/* Slot 3: Left Full */
/* Slot 4: Right Full */

/* CONSTANTS
 *
 * field length from start to end: 356.87 cm
 * length of one tile: 59.478 cm
 * 
 *
 */

#include <string.h>
#include <cassert>

#include "global.h"
#include "chassis.hpp"
#include "arm.hpp"


/* TODO:
 * Automating script for code uploading to all slots
 */

#define ROBOT_SKILLS_SLOT 9
#define HARDCODED_AUTONOMOUS 1

#define AUTO_DELAY 100

void hardcoded_autonomous_fn(void) {						
	
	#if HARDCODED_AUTONOMOUS == 1
		#pragma message("Autonomous: 1, hardcoded")
		master.set_text(0, 0, "otonom 1 abi");
		chassis->move_relative(157, 132, MAX_CHASSIS_VEL);
		delay(50);
		chassis->move_relative(4, 4, MAX_CHASSIS_VEL);
		delay(100);
		arm->move_absolute(115, 70);
		delay(2000);
		chassis->turn_around(-35);
		chassis->move_relative(-100, -115, MAX_CHASSIS_VEL);
	#elif HARDCODED_AUTONOMOUS == 2
		#pragma message("Autonomous: 2, hardcoded")
		master.set_text(0, 0, "otonom 2 abi");
		chassis->move_relative(143, 143, MAX_CHASSIS_VEL);
		delay(AUTO_DELAY);
		arm->move_absolute(115, 127);
		delay(2000);
		chassis->move_relative(-(143 - 51.1 ), - (143 - 51.1 ), MAX_CHASSIS_VEL); // 138 path length, 27 length of the robot's back, 51 for avoiding hitting the ramp
	#elif HARDCODED_AUTONOMOUS == 3
		#pragma message("Autonomous: 3, hardcoded")
		master.set_text(0, 0, "otonom 3 abi");
		chassis->move_relative(138, MAX_CHASSIS_VEL);
		delay(AUTO_DELAY);
		chassis->turn_around(34);
		delay(AUTO_DELAY);
		chassis->move_relative(9, MAX_CHASSIS_VEL);
		delay(AUTO_DELAY);
		arm->lift_up_down(ARM_UP);
		delay(AUTO_DELAY);
		chassis->turn_around(-23)		;
		delay(AUTO_DELAY);
		arm->move_absolute(0, 127);
		chassis->move_relative(-(158 - 27 - 51.1), MAX_CHASSIS_VEL);
	#elif HARDCODED_AUTONOMOUS == 4
		#pragma message("Autonomous: 4, hardcoded")
		master.set_text(0, 0, "otonom 4 abi");
		chassis->move_relative(143, 143, MAX_CHASSIS_VEL);
		delay(50);
		delay(AUTO_DELAY);
		arm->lift_up_down(ARM_UP);
		chassis->turn_around(-90);
		arm->move_absolute(10, MAX_ARM_VEL * 4 / 5);
		chassis->move_relative(150, 150, MAX_CHASSIS_VEL); // 138 path length, 27 length of the robot's back, 51 for avoiding hitting the ramp
	#else
		#pragma error("NO VALID SLOT GIVEN")
	#endif
}

#endif
