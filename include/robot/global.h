#ifndef GLOBAL_H
#define GLOBAL_H

#define PROS_USE_LITERALS
#define PROS_USE_SIMPLE_NAMES

#include <iostream>
#include "../api.h"

using namespace pros;

#define PI 3.1415926535897932384626433

#define MAX_VEL 127
#define RADIO_PORT 3
#define IMU_PORT 8
#define CCYCLE 15
#define TILE_LENGTH 59.478 // in centimeters

#define DELAY CCYCLE

Imu imu(IMU_PORT);
Controller master(E_CONTROLLER_MASTER);
Mutex m_controller;


const char WP[] = { // working ports
	2, // sol orta
	3,
	5,
	6,
	8,
	9, 
	13,
	14,
	16,
	17,
	18, // sağ orta
	19,
	21
};

void print(const char *msg, bool clear=false);
#endif
