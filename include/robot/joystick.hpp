#ifndef JOYSTICK_H
#define JOYSTICK_H

#include "../api.h"
#include "global.h"

using namespace pros;

struct JoystickState {
   	int LX;
   	int LY;
   	//int RX;
   	int RY;
   	bool X: 	1;
   	bool Y: 	1;
   	bool A: 	1;
   	bool B: 	1;
   	bool L1:	1;
   	bool L2:	1;
   	bool R1:	1;
   	bool R2:	1;
   	bool UP:	1;
   	bool DOWN:	1;
   	bool LEFT:	1;
   	bool RIGHT:	1;
};

/* sizeof(JoystickState) == 16 */
/* 16 * 40(tickrate) * 15(autonomous length) = 9600 */
JoystickState* autonomous_buffer = new JoystickState[AUTO_TOTAL_TICKS]();	

struct Joystick {
   	JoystickState state;
   	Task *task;

   	int get_analog(controller_analog_e_t  _analog) {
   		int result;
   		m_controller.take();

   		switch (_analog) {
   			case E_CONTROLLER_ANALOG_LEFT_X:
   				result = state.LX;
   				break;

   			case E_CONTROLLER_ANALOG_LEFT_Y:
   				result = state.LY;
   				break;

   			/* case E_CONTROLLER_ANALOG_RIGHT_X: */
   			/* 	result = state.RX; */
   			/* 	break; */

   			case E_CONTROLLER_ANALOG_RIGHT_Y:
   				result = state.RY;
   				break;

   			default:
   				result = 0;
   				break;
   		}
   		m_controller.give();
   		return result;
   	}

   	int get_button(controller_digital_e_t _button) {
   		int result;
   		m_controller.take();
   		switch (_button) {
   			case E_CONTROLLER_DIGITAL_Y:
   				result = state.Y;
   				break;

   			case E_CONTROLLER_DIGITAL_X:
   				result = state.X;
   				break;

   			case E_CONTROLLER_DIGITAL_A:
   				result = state.A;
   				break;

   			case E_CONTROLLER_DIGITAL_B:
   				result = state.B;
   				break;

   			case E_CONTROLLER_DIGITAL_L1:
   				result = state.L1;
   				break;

   			case E_CONTROLLER_DIGITAL_L2:
   				result = state.L2;
   				break;

   			case E_CONTROLLER_DIGITAL_R1:
   				result = state.R1;
   				break;

   			case E_CONTROLLER_DIGITAL_R2:
   				result = state.R2;
   				break;

   			case E_CONTROLLER_DIGITAL_UP:
   				result = state.UP;
   				break;

   			case E_CONTROLLER_DIGITAL_DOWN:
   				result = state.DOWN;
   				break;

   			case E_CONTROLLER_DIGITAL_LEFT:
   				result = state.LEFT;
   				break;

   			case E_CONTROLLER_DIGITAL_RIGHT:
   				result = state.RIGHT;
   				break;

   			default:
   				result = -1;
				break;
   		}
   		m_controller.give();
   		return result;
   	}

   	void update_state(void)  {
   		static int auto_counter = 0;
   		m_controller.take();
   		if (!competition::is_autonomous()) {
   			
   			state.LX = master.get_analog(E_CONTROLLER_ANALOG_LEFT_X);
   			state.LY = master.get_analog(E_CONTROLLER_ANALOG_LEFT_Y);
   			/* state.RX =  master.get_analog(ANALOG_RIGHT_X); */
   			state.RY = master.get_analog(E_CONTROLLER_ANALOG_RIGHT_Y);
   			state.X = (bool) master.get_digital_new_press(E_CONTROLLER_DIGITAL_X);
   			state.Y = (bool) master.get_digital_new_press(E_CONTROLLER_DIGITAL_Y);
   			state.A = (bool) master.get_digital_new_press(E_CONTROLLER_DIGITAL_A);
   			state.B = (bool) master.get_digital_new_press(E_CONTROLLER_DIGITAL_B);
   			state.L1 = (bool) master.get_digital_new_press(E_CONTROLLER_DIGITAL_L1);
   			state.L2 = (bool) master.get_digital_new_press(E_CONTROLLER_DIGITAL_L2);
   			state.R1 = (bool) master.get_digital_new_press(E_CONTROLLER_DIGITAL_R1);
   			state.R2 = (bool) master.get_digital_new_press(E_CONTROLLER_DIGITAL_R2);
   			state.UP = (bool) master.get_digital(E_CONTROLLER_DIGITAL_UP);
   			state.DOWN = (bool) master.get_digital(E_CONTROLLER_DIGITAL_DOWN);
   			state.LEFT = (bool) master.get_digital(E_CONTROLLER_DIGITAL_LEFT);
   			state.RIGHT = (bool) master.get_digital(E_CONTROLLER_DIGITAL_RIGHT);
   			
   			if (is_recording) {
   					if (state.UP)
   						state.LY = 85;
   					else if (state.DOWN)
   						state.LY = -85;

   					state.UP = state.DOWN = false;
   			}
   		}else
   			state = autonomous_buffer[auto_counter++];

   		m_controller.give();
   	}

   	static void handler_task(void *_joystick) {
   		Joystick *joystick = (Joystick *) _joystick;
   		delay(50);
   		uint32_t time = millis();
   		while (true) {
   			joystick->update_state();
   			notify_tasks();
   			Task::delay_until(&time, DELAY);
   		}
   	}

   	Joystick(void) {
   		task = new Task (handler_task, this);
		state = {0};
   	}

	void print_controller_state(void) {
		printf("LY: %d\n",	 get_analog( E_CONTROLLER_ANALOG_LEFT_Y  ));
		printf("LX: %d\n",	 get_analog( E_CONTROLLER_ANALOG_LEFT_X  ));
		printf("RY: %d\n",	 get_analog( E_CONTROLLER_ANALOG_RIGHT_Y ));
		printf("X: %d\n", 	 get_button( E_CONTROLLER_DIGITAL_X	     ));
		printf("Y: %d\n", 	 get_button( E_CONTROLLER_DIGITAL_Y	     ));
		printf("A: %d\n", 	 get_button( E_CONTROLLER_DIGITAL_A	     ));
		printf("B: %d\n", 	 get_button( E_CONTROLLER_DIGITAL_B	     ));
		printf("L1: %d\n",	 get_button( E_CONTROLLER_DIGITAL_L1     ));
		printf("L2: %d\n",	 get_button( E_CONTROLLER_DIGITAL_L2	 ));
		printf("R1: %d\n",	 get_button( E_CONTROLLER_DIGITAL_R1     ));
		printf("R2: %d\n",	 get_button( E_CONTROLLER_DIGITAL_R2     ));
		printf("Up: %d\n",   get_button( E_CONTROLLER_DIGITAL_UP     ));
		printf("Down: %d\n", get_button( E_CONTROLLER_DIGITAL_DOWN   ));
		printf("Left: %d\n", get_button( E_CONTROLLER_DIGITAL_LEFT   ));
		printf("Right: %d\n",get_button( E_CONTROLLER_DIGITAL_RIGHT  ));
	}

} *controller = new Joystick();

#endif
