#!/bin/sh

ROBOT_NAME="ET"
ROBOT_DESCR="Eagle Tech Tipping Point 2022"

DEF_PORT="1"
DEF_ACT="none"

pros make 
sudo $(which pros) upload --slot $( \
									python -c "print(int(\'$1\') or int(\'$2\') or int(\'$DEF_PORT\'))"\
									) \
							--name $ROBOT_NAME \
							--description $ROBOT_DESCR \
							--verbose \
							--after $(if [[ $1 == "r" || $2 == "r" ]]; then echo -n run; else echo -n $DEF_ACT; fi)
